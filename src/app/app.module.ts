import {BrowserModule} from '@angular/platform-browser'
import {LOCALE_ID, NgModule, Type} from '@angular/core'

import {AppComponent} from './app.component'
import {BookService} from './shared/services/book.service'
import {HttpClientModule} from '@angular/common/http'
import {HomeContainer} from './containers/home/home.container'
import {RouterModule} from '@angular/router'
import {appRoutes} from './app-routing.module'
import {BookComponent} from './components/book/book.component'
import localeFr from '@angular/common/locales/fr'
import {registerLocaleData} from '@angular/common'
import {BasketService} from './shared/services/basket.service'
import {BasketContainer} from './containers/basket/basket.container'
import {DetailContainer} from './containers/detail/detail.container'
import {ErrorContainer} from './containers/error/error.container'
import {DetailResolverService} from './containers/detail/detail-resolver.service'
import {FormsModule} from '@angular/forms'
import {FilterPipe} from './shared/pipes/filter.pipe'
import {BASE_URL} from './app'

registerLocaleData(localeFr)

export const COMPONENTS: Type<any>[] = [
  AppComponent,
  BookComponent
]

export const CONTAINERS: Type<any>[] = [
  HomeContainer,
  BasketContainer,
  DetailContainer,
  ErrorContainer
]

export const PIPES: Type<any>[] = [
  FilterPipe
]

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  declarations: [
    ...COMPONENTS,
    ...CONTAINERS,
    ...PIPES
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    {provide: BASE_URL, useValue: 'http://henri-potier.xebia.fr'},
    BookService,
    BasketService,
    DetailResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

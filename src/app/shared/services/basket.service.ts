import {Inject, Injectable} from '@angular/core'
import {Book, Books} from '../models/book'
import {Observable, Subject, throwError} from 'rxjs'
import {CommercialOffer, CommercialOffers} from '../models/commercial-offers'
import {catchError} from 'rxjs/operators'
import {HttpClient, HttpErrorResponse} from '@angular/common/http'
import {CommercialOfferType} from '../enums/commercial-offer'
import {BASE_URL} from '../../app'

@Injectable()
export class BasketService {

  private _basket: Books = []
  private _endPoint: string = '/books'
  private _totalPriceBrut: number = 0

  constructor(
    private _httpClient: HttpClient,
    @Inject(BASE_URL) private _baseUrl: string
  ) {

  }

  get count(): number {
    return this.basket.length
  }

  get basket(): Books {
    return this._basket
  }

  get totalPriceBrut(): number {
    return this._totalPriceBrut
  }

  add(book: Book): void {
    this._basket.push(book)
    this._updateTotalPrice()
  }

  remove(bookIndex: number): void {
    if (this._basket.length > -1) {
      this._basket.splice(bookIndex, 1)
    }
    this._updateTotalPrice()
  }

  clear(): void {
    this._basket = []
  }

  calculateTotalPrice(): Observable<number> {
    const isbnList: string[] = this.basket.map((book: Book) => book.isbn)
    const subject: Subject<number> = new Subject()

    if (isbnList.length > 0) {
      this.getCommercialOffers(isbnList)
        .subscribe((commercialOffers: CommercialOffers) => {
          const bestCommercialOfferType: CommercialOffer | undefined = this.findBestCommercialOffer(commercialOffers)
          const totalPrice: number = this.applyCommercialOffer(bestCommercialOfferType)

          return subject.next(totalPrice)
        })
    }

    return subject
  }

  getCommercialOffers(isbnList: string[]): Observable<CommercialOffers> {
    const isbnPathParams = isbnList.join(',')
    return this._httpClient.get<CommercialOffers>(`${this._baseUrl}${this._endPoint}/${isbnPathParams}/commercialOffers`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return throwError(error)
        })
      )
  }

  private _updateTotalPrice(): void {
    this._totalPriceBrut = this.basket
      .reduce((accumulator: number, currentValue: Book) => accumulator + currentValue.price, 0)
  }

  public applyCommercialOffer(commercialOffer: CommercialOffer = <CommercialOffer>{}): number {
    switch (commercialOffer.type) {
      case CommercialOfferType.PERCENTAGE:
        return this.totalPriceBrut * (100 - commercialOffer.value) / 100
      case CommercialOfferType.MINUS:
        return this.totalPriceBrut - commercialOffer.value
      case CommercialOfferType.SLICE:
        return this.totalPriceBrut - Math.floor(this.totalPriceBrut / commercialOffer.sliceValue) * commercialOffer.value
      default:
        return this.totalPriceBrut
    }
  }

  public findBestCommercialOffer(commercialOffers: CommercialOffers): CommercialOffer | undefined {
    const { offers } = commercialOffers

    if (offers.length) {
      const commercialOffersPrices: number[] = offers
        .map((commercialOffer: CommercialOffer) => this.applyCommercialOffer(commercialOffer))
      return offers[commercialOffersPrices.indexOf(Math.min(...commercialOffersPrices))]
    }

    return
  }

}

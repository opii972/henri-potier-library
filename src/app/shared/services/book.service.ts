import {Inject, Injectable} from '@angular/core'
import {Observable, throwError} from 'rxjs'
import {Book, Books} from '../models/book'
import {HttpClient, HttpErrorResponse} from '@angular/common/http'
import {catchError, defaultIfEmpty, filter, flatMap} from 'rxjs/operators'
import {BASE_URL} from '../../app'

@Injectable()
export class BookService {

  private _endPoint: string = '/books'

  constructor(
    private _httpClient: HttpClient,
    @Inject(BASE_URL) private _baseUrl: string
  ) {

  }

  public getBooks(): Observable<Books> {
    return this._httpClient.get<Books>(`${this._baseUrl}${this._endPoint}`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return throwError(error)
        })
      )
  }

  public getBook(isbn: string): Observable<Book> {
    return this.getBooks()
      .pipe(
        flatMap((data: Books) => data),
        filter((book: Book) => book.isbn === isbn),
        defaultIfEmpty(<Book>{})
      )
  }

}

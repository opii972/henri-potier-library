export function isEmpty<T>(object: T) {
  return Object.keys(object).length === 0 && object.constructor === Object
}

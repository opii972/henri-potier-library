export enum CommercialOfferType {
  PERCENTAGE = 'percentage',
  MINUS = 'minus',
  SLICE = 'slice'
}

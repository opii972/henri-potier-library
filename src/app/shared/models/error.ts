import {HttpStatusCode} from '../enums/http-status-code'

export interface ErrorTemplate {
  title: string
  description: string
}

export abstract class ErrorPage {
  abstract getTemplate(): ErrorTemplate
}

export class PageNotFound extends ErrorPage {
  getTemplate(): ErrorTemplate {
    return {
      title: '404 Error',
      description: `Oops something wrong! Looks like this content doesn't exist`
    }
  }
}

export class ErrorPageFactory {
  static createErrorPage(type: HttpStatusCode): ErrorPage {
    switch (type) {
      case HttpStatusCode.NOT_FOUND:
        return new PageNotFound()
      default:
        return null // Technical Error
    }
  }
}

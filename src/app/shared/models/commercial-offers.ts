import {CommercialOfferType} from '../enums/commercial-offer'

export interface CommercialOffers {
  offers?: CommercialOffer[]
}

export interface PercentageOffer {
  type: 'percentage'
  value: number
}

export interface MinusOffer {
  type: 'minus'
  value: number
}

export interface SliceOffer {
  type: 'slice'
  sliceValue: number
  value: number
}

export type CommercialOffer = PercentageOffer | MinusOffer | SliceOffer

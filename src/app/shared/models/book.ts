export interface Book {
  isbn: string;
  title: string;
  price: number;
  cover: string;
  synopsis: string[];
}

export type Books = Book[];

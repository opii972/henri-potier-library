import {TestBed, async} from '@angular/core/testing'
import {AppComponent} from './app.component'
import {
  RouterTestingModule
} from '@angular/router/testing'
import {appRoutes} from './app-routing.module'
import {HomeContainer} from './containers/home/home.container'
import {DetailContainer} from './containers/detail/detail.container'
import {BasketContainer} from './containers/basket/basket.container'
import {ErrorContainer} from './containers/error/error.container'
import {FormsModule} from '@angular/forms'
import {FilterPipe} from './shared/pipes/filter.pipe'
import {BookComponent} from './components/book/book.component'
import {BasketService} from './shared/services/basket.service'
import {HttpClientModule} from '@angular/common/http'
import {BASE_URL} from './app'

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeContainer,
        DetailContainer,
        BasketContainer,
        ErrorContainer,
        FilterPipe,
        BookComponent
      ],
      imports: [
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(appRoutes)
      ],
      providers: [
        BasketService,
        {provide: BASE_URL, useValue: 'http://henri-potier.xebia.fr'}
      ]
    }).compileComponents()
  }))
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  }))
  it(`should have as title 'Henri Potier Library'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app.title).toEqual('Henri Potier Library')
  }))
})

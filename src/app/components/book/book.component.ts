import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core'
import {Book} from '../../shared/models/book'

@Component({
  selector: 'hp-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'hp-book'
  }
})
export class BookComponent {

  @Input()
  book: Book

}

import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core'
import {Book, Books} from '../../shared/models/book'
import {BookService} from '../../shared/services/book.service'
import {BasketService} from '../../shared/services/basket.service'
import {ActivatedRoute, Router} from '@angular/router'
import {Subscription} from 'rxjs'
import {StaticPageData} from '../../app-routing.module'

@Component({
  templateUrl: './home.container.html',
  styleUrls: ['./home.container.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeContainer implements OnInit, OnDestroy {

  private _books: Books
  private _subscriptions: Subscription[] = []
  private _title: string
  private _query: string

  constructor(
    private _bookService: BookService,
    private _basketService: BasketService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
  }

  get title(): string {
    return this._title
  }

  get books(): Books {
    return this._books
  }

  get query(): string {
    return this._query
  }

  set query(value: string) {
    this._query = value
  }

  ngOnInit(): void {
    this._subscriptions.push(
      this._route
        .data
        .subscribe((data: StaticPageData) => {
          this._title = data.title
        })
    )

    this._subscriptions.push(
      this._bookService.getBooks()
        .subscribe((books: Books) => {
          this._books = books
        })
    )
  }

  _readMore(book: Book): void {
    this._router.navigate(['/detail', book.isbn])
  }

  _addToBasket(book: Book): void {
    this._basketService.add(book)
  }

  _clearFilter(): void {
    this._query = ''
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe())
  }

}

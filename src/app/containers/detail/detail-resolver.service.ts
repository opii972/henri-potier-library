import {Injectable} from '@angular/core'
import {BookService} from '../../shared/services/book.service'
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router'
import {Book} from '../../shared/models/book'
import {EMPTY, Observable, of} from 'rxjs'
import {mergeMap, take} from 'rxjs/operators'
import {isEmpty} from '../../shared/utils/object'

@Injectable({
  providedIn: 'root'
})
export class DetailResolverService implements Resolve<Book> {

  constructor(
    private _router: Router,
    private _bookService: BookService
  ) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Book> | Observable<never> {
    let id: string = route.paramMap.get('id')

    return this._bookService.getBook(id)
      .pipe(
        take(1),
        mergeMap((book: Book) => {
          if (!isEmpty(book)) {
            return of(book)
          } else {
            this._router.navigate(['/error'])
            return EMPTY
          }
        })
      )
  }
}

import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {BookService} from '../../shared/services/book.service'
import {Book} from '../../shared/models/book'
import {Subscription} from 'rxjs'
import {BasketService} from '../../shared/services/basket.service'

@Component({
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'hp-detail'
  }
})
export class DetailContainer implements OnInit, OnDestroy {

  private _book: Book
  private _subscriptions: Subscription[] = []

  constructor(
    private _route: ActivatedRoute,
    private _bookService: BookService,
    private _basketService: BasketService
  ) {
  }

  get book(): Book {
    return this._book
  }

  ngOnInit(): void {
    this._subscriptions.push(
      this._route.data.subscribe((data: {book: Book}) => {
        this._book = data.book
      })
    )
  }

  _addToBasket(book: Book): void {
    this._basketService.add(book)
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe())
  }

}

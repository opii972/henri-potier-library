import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core'
import {BasketService} from '../../shared/services/basket.service'
import {Books} from '../../shared/models/book'
import {Subscription} from 'rxjs'
import {ActivatedRoute} from '@angular/router'
import {StaticPageData} from '../../app-routing.module'

@Component({
  templateUrl: './basket.container.html',
  styleUrls: ['./basket.container.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'hp-basket'
  }
})
export class BasketContainer implements OnInit, OnDestroy {

  private _subscriptions: Subscription[] = []
  private _title: string
  private _totalPrice: number

  constructor(
    private _basketService: BasketService,
    private _route: ActivatedRoute
  ) {
  }

  get title(): string {
    return this._title
  }

  get basket(): Books {
    return this._basketService.basket
  }

  get totalPrice(): number {
    return this._totalPrice
  }

  _remove(bookIndex: number): void {
    this._basketService.remove(bookIndex)
    this.updateTotalPrice()
  }

  _clear(): void {
    this._basketService.clear()
    this.updateTotalPrice()
  }

  ngOnInit(): void {
    this._subscriptions.push(
      this._route
        .data
        .subscribe((data: StaticPageData) => {
          this._title = data.title
        })
    )

    this.updateTotalPrice()
  }

  updateTotalPrice(): void {
    this._subscriptions.push(
      this._basketService.calculateTotalPrice()
        .subscribe((totalPrice: number) => this._totalPrice = totalPrice)
    )
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe())
  }

}

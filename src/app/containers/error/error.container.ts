import {Component, OnDestroy, OnInit} from '@angular/core'
import {StaticErrorPageData} from '../../app-routing.module'
import {ActivatedRoute} from '@angular/router'
import {Subscription} from 'rxjs'
import {ErrorPageFactory, ErrorTemplate} from '../../shared/models/error'

@Component({
  templateUrl: './error.container.html'
})
export class ErrorContainer implements OnInit, OnDestroy {

  private _subscriptions: Subscription[] = []
  private _template: ErrorTemplate

  constructor(
    private _route: ActivatedRoute
  ) {

  }

  get template(): ErrorTemplate {
    return this._template
  }

  ngOnInit(): void {
    this._subscriptions.push(
      this._route
        .data
        .subscribe((data: StaticErrorPageData) => {
          this._template = ErrorPageFactory.createErrorPage(data.errorCode).getTemplate()
        })
    )
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe())
  }

}


import {Component} from '@angular/core'
import {BasketService} from './shared/services/basket.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Henri Potier Library'

  constructor(private _basketService: BasketService) {

  }

  get count(): number {
    return this._basketService.count
  }
}

import {InjectionToken} from '@angular/core'

// Move into separated file to avoid circular dependencies
export const BASE_URL: InjectionToken<string> = new InjectionToken<string>('BaseUrl')
